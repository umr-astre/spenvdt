from era5_zonal_stats_multiproc import calculation_process_huge_polygon_multiproc, save_geobject
from era5_download_multithread import download_all_variables_data
from tools.vector import boundingbox, open_vector
import pandas as pd
import geopandas as gpd
from tools.control_os import whatpathtype
from multiprocessing import freeze_support
from tools.meteofranceformat import save_txt_default, meteofrance_rearange_output

whatpathtype()



def ERA5_dlandprocess(variables, starttime, endtime, path, vector_path, vector_column_to_keep, **kargs):

    defaultKwargs = {'savefileSHP': True, 'savefileTXT': True, 'savefileTXT_Path': None}
    kargs = {**defaultKwargs, **kargs}

    stats = "mean"
    vector = open_vector(vector_path)
    bbox_vector = boundingbox(vector)

    #download_all_data
    files_list = download_all_variables_data(variables, starttime, endtime, path, bbox_vector)

    output = pd.DataFrame()
    geobject = gpd.GeoDataFrame()

    #Loop on each variables selected
    for variable in variables :
        #Open the file_list dictionary for the complete list of Posixpath of files previously selected

        geobject = calculation_process_huge_polygon_multiproc(vector, files_list[variable], stats,
                                                                 limit_polygon=15000, vector_column_to_keep=vector_column_to_keep)

        if kargs['savefileSHP'] == True:
            save_geobject(vector_path, geobject, stats)

        if kargs['savefileTXT'] == True:
            outrearrange = meteofrance_rearange_output(geobject, vector_column_to_keep, variable)

            try:
                output = pd.merge(output, outrearrange, how='inner')
            except:
                output = outrearrange

    if kargs['savefileTXT'] == True:
        if kargs['savefileTXT_Path'] == None:
                save_txt_default(vector_path, output, stats)  # save the data in file
        else:
            output.to_csv(f"{kargs['savefileTXT_Path']}")

    return geobject, output


#Windows protection for the multiprocessing
"""
The purpose of using if __name__ == '__main__': is to ensure that any code that follows this statement will only
be executed if the script is being run as the main program.
This is important when using multiprocessing because each process will import the module containing the multiprocessing
code, and you don't want the same code to be executed multiple times.
"""

if __name__ == '__main__':

    ### Download elements

    starttime = '2022-03-01'
    endtime = '2022-03-31'

    # variable to sudy, by default : ['t2m', 'tp']
    variables = ['t2m', 'tp']
    path = 'D:/Pachka_tmp/1-Modeling/spenvdt'

    ### Shared object
    vector_path = 'D:/Pachka_tmp/1-Modeling/spenvdt/reprojectedPARCELLES.shp'

    ### Processing_elements
    vector_column_to_keep = 'ID'


    freeze_support()
    geobject, output = ERA5_dlandprocess(variables, starttime, endtime, path, vector_path, vector_column_to_keep)
