from threading import Thread
import cdsapi
from tools.vector import boundingbox, open_vector
from pathlib import Path
import pandas as pd
import urllib3


urllib3.disable_warnings() # disable warnings from copernicus HTTPS... problem from their side.


global c
c = cdsapi.Client()


class ThreadWithReturnValue(Thread):
    #Class and functions to get the value from the the Thread result execution
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args,
                                        **self._kwargs)

    def join(self, *args):
        Thread.join(self, *args)
        return self._return

def era5download(variable, year, month, pathtodownload, bbox):

    #global path_file # to get the value under the function when use multithread

    path_file = Path(f"{pathtodownload}/era5_{variable}_{year}_{month}_{str(bbox['ymax']).split('.')[0] + '_' + str(bbox['ymax']).split('.')[1]}_{str(bbox['xmin']).split('.')[0] + '_' + str(bbox['xmin']).split('.')[1]}_{str(bbox['ymin']).split('.')[0] + '_' + str(bbox['ymin']).split('.')[1]}_{str(bbox['xmax']).split('.')[0] + '_' + str(bbox['xmax']).split('.')[1]}.nc")
    if path_file.is_file() == False:

        if variable == 'tp':
            var = 'Total precipitation'
        elif variable == 't2m':
            var = '2m_temperature'

        c.retrieve(
            'reanalysis-era5-land',
            {
                'variable': [var],
                'area': [
                    bbox['ymax'],bbox['xmin'],
                    bbox['ymin'], bbox['xmax']
                ],
                'format': 'netcdf',
                'day': [
                    '01', '02', '03',
                    '04', '05', '06',
                    '07', '08', '09',
                    '10', '11', '12',
                    '13', '14', '15',
                    '16', '17', '18',
                    '19', '20', '21',
                    '22', '23', '24',
                    '25', '26', '27',
                    '28', '29', '30',
                    '31',
                ],
                'time': [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00',
                ],
                'year': str(year),
                'month': str(month),
            },
            f"{pathtodownload}/era5_{variable}_{year}_{month}_{str(bbox['ymax']).split('.')[0]+'_'+str(bbox['ymax']).split('.')[1]}_{str(bbox['xmin']).split('.')[0]+'_'+str(bbox['xmin']).split('.')[1]}_{str(bbox['ymin']).split('.')[0]+'_'+str(bbox['ymin']).split('.')[1]}_{str(bbox['xmax']).split('.')[0]+'_'+str(bbox['xmax']).split('.')[1]}.nc")

        print(f"Download OK for {variable} on year {year} and month {month}")
        return path_file

    else :
        print(f"The file already exist for {variable} on year {year} and month {month}")
        return path_file

def select_month_year(starttime,endtime):
    listyearsmonth = pd.DataFrame(pd.date_range(starttime, endtime,
                                                freq='MS').strftime("%Y-%m"))
    listyearsmonth_splited = listyearsmonth[0].str.split("-", expand=True)

    return listyearsmonth_splited

def define_folder_per_variable(variable, path):
    ## creating folders
    if variable == 't2m':
        return  f"{path}/era5_t2m"
    elif variable == 'tp':
        return  f"{path}/era5_tp"
    else:
        return "The variable is actually define to be '2m_temperature' or 'Total precipitation'"



def download_all_data(variable, starttime, endtime, path, bbox):
    ## creating folders
    path_folder = define_folder_per_variable(variable, path)
    
    print(path_folder)

    Path(path_folder).mkdir(exist_ok=True) #create a folder if not exist

    listyearsmonth = select_month_year(starttime, endtime)

    #download all data by month (impossible to increase the temporal envelop with the ERA5 land hourly product)
    # Collect the threads
    threads = []
    path_file_list = []

    for yearmonth in listyearsmonth.values:
        #download_thread = Thread(target=era5download, args=(variable, yearmonth[0], yearmonth[1], path_folder, bbox)) #need to define a global variable in the function era5download named path_file

        download_thread = ThreadWithReturnValue(target=era5download, args=(variable, yearmonth[0], yearmonth[1], path_folder, bbox))
        threads.append(download_thread)

    # Start them all
    for thread in threads:
        thread.start()
        #path_file_list.append(path_file) #linked to the global variable in the function era5download

    # Wait for all to complete
    for thread in threads:
        path_file = thread.join()
        path_file_list.append(path_file)

    return path_file_list


def download_all_variables_data(variables, starttime, endtime, path, bbox):
    full_list = {}
    for variable in variables :
        list = download_all_data(variable, starttime, endtime, path, bbox)
        full_list[variable] = list

    print("Download successfull")
    return full_list



"""
starttime = "2020-01-01"
endtime = '2022-12-31'
#'2m_temperature' ===> t2m
#'Total precipitation' ===> tp
variables = ['t2m', 'tp']
path = 'data/files_tests/era5_netcdf'
vector_path = "data/files_tests/IRIS/IRISGE_MTP.shp"
file_list = download_all_variables_data(variables, starttime, endtime, path, boundingbox(open_vector(vector_path)))
"""
