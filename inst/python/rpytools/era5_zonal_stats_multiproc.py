import rasterstats as rstats
import geopandas as gpd
import numpy as np
import xarray as xr
from pathlib import Path, PosixPath, WindowsPath
from multiprocessing import Pool
import pandas as pd
import warnings
from shapely.errors import ShapelyDeprecationWarning
from tools.vector import open_vector, write_geobject
import traceback
from tools.control_os import whatpathtype
from tools.cpu_check import limit_cpu
from tools.netcdf_process import list_netcdf

warnings.filterwarnings("ignore", category=ShapelyDeprecationWarning)  # warning with module shapely in rasterstats

def conversion(netcdfobj, var):
    """
    @param netcdfobj: The netcdf object open with xarray
    @param var: the name of the var in the netcdf, can be used exclusively with a netcdf with one var
    @return: the corrected values with corresponding conversion for a better values interpretation
    """
    if var == 't2m':  # temperatur at 2 meter
        return netcdfobj.values - 273.15  # Kalvin to Degres C
    elif var == 'tp':  # total precipitation
        return netcdfobj.values * 1000  # M in mm
    else:
        return netcdfobj.values


def resample_day_precipitation(geobject):

    for key in geobject.keys():
        try:
            temp = pd.to_datetime(key)
            temps2 = temp + pd.offsets.Day(-1)
            temps3 = str(temps2).split(" ")[0]
            geobject = geobject.rename(columns={key: temps3})
        except:
            pass

    return geobject


def calculate_zonal_stats_netcdf(vectorobj, netcdfobj, select_date, stats):
    """
    @param vectorobj:
    @param netcdfobj:
    @param select_date:
    @param stats:
    @return:
    """
    affine = netcdfobj.rio.transform()
    result = netcdfobj.sel(time=select_date)
    result_values = result.values

    result_stat = rstats.zonal_stats(vectorobj.geometry, result_values, affine=affine, stats=stats, all_touched=True,
                                     geojson_out=True,
                                     nodata=-999.9)  # nodata à rendre modifiable dans les paramètres de la fonction
    date_to_insert_inheader = str(np.datetime64(select_date, 'D'))

    geobject = gpd.GeoDataFrame.from_features(result_stat)
    geobject = geobject.rename(columns={stats: date_to_insert_inheader})


    return geobject
  
def calcul_zonal_nc_multiproc(vectorobj, ntcdfpath, stats):

    #Test if it's a path or a list of path in WindowsPath format
    if type(ntcdfpath) == type(str()):

        try :
            ncfiles = list_netcdf(ntcdfpath)

        except Exception:
            return traceback.print_exc()

    elif type(ntcdfpath) == type(list()):


        typepath = whatpathtype()

        if type(ntcdfpath[0]) == typepath:
                ncfiles = ntcdfpath

        else:
            raise TypeError (print('The list entry is not in WindowsPath format (package pathlib)'))

    else :
        raise TypeError (print('The "ntcdfpath" parameter must be a path to your folder or the list of your nectdf file in WindowsPath format (package pathlib)'))


    var = []

    for pathnc in ncfiles:
        nc_file = xr.open_dataset(pathnc)

        var = [i for i in nc_file.data_vars][0]  # liste of all variables (execpt time,lat, lon), select the first
        netcdfslice = nc_file[var]  # select the first var

        if 'expver' in netcdfslice.dims:  # this part is for resolv the issue with era5 and era5T in the same netcdf
            netcdfslice = netcdfslice.sel(expver=1).combine_first(netcdfslice.sel(expver=5))
            netcdfslice.load()


        if var == 'tp':
            netcdfslice = netcdfslice.drop_sel(time=[netcdfslice.time.values[0]])
            tp_spatial_per_day = netcdfslice.resample(indexer=dict(time='24H')).first()
            #https://stackoverflow.com/questions/58881320/how-manipulate-era5-land-hourly-data-with-python
            #tp_spatial_per_day = tp_spatial_per_day.shift(shifts=dict(time= -1))
            #tp_spatial_per_day = tp_spatial_per_day.drop_sel(time=tp_spatial_per_day['time'].isel(time=len(tp_spatial_per_day['time'])-1).values)
            #https://github.com/loicduffar/era5-tools
        else:
            tp_spatial_per_day = netcdfslice.resample(time='1D').mean('time')

        tp_spatial_per_day = tp_spatial_per_day.rio.set_spatial_dims(x_dim='longitude', y_dim='latitude')
        tp_spatial_per_day = tp_spatial_per_day.rio.write_crs("epsg:4326", inplace=True)  # define the crs


        tp_spatial_per_day.values = conversion(tp_spatial_per_day,
                                               var)  # convert the values in the good format ! (Kavlin to Degree C by example)

        data_dates = tp_spatial_per_day['time'].values

        geobject = vectorobj.to_crs(tp_spatial_per_day.rio.crs)


        tuple_list = []
        date_list = []

        with Pool(None, limit_cpu) as pool:
            for select_date in data_dates:
                date_to_insert_inheader = str(np.datetime64(select_date, 'D'))
                date_list.append(date_to_insert_inheader)

                variables_tuple = (geobject, tp_spatial_per_day, select_date, stats)
                # issue tasks to the process pool
                tuple_list.append(variables_tuple)

            #Execute all the list of variable directly in tuple
            #Create a pool of process executed simultaneaously
            result = pool.starmap_async(calculate_zonal_stats_netcdf, tuple_list)

            #https://superfastpython.com/multiprocessing-pool-starmap_async/
            #https://superfastpython.com/multiprocessing-pool-issue-tasks/#How_to_Use_Poolstarmap_async

            #result.get is the list result of each process of the pool
            x = 0
            for geobject_list_date in result.get():
                geobject[date_list[x]] = geobject_list_date[date_list[x]]
                x += 1

            # close the process pool
            pool.close()
            # wait for all tasks to complete
            pool.join()

            try:
                geobject_merge = pd.merge(geobject_merge, geobject)
            except:
                geobject_merge = geobject

            print(f'Calcul on {pathnc} ok')


    geobject_merge['var'] = var
    return geobject_merge


def calculation_process_huge_polygon_multiproc(vector, netcdf_folder_path, stats, **kargs):

    defaultKwargs = {'limit_polygon': 1500, 'vector_column_to_keep': None}
    kargs = {**defaultKwargs, **kargs}

    geobject = gpd.GeoDataFrame()

    #open vector path as geodataframe if not alreay a geodataframe
    if type(vector) != type(gpd.GeoDataFrame()):
        vector = open_vector(vector)


    if kargs['vector_column_to_keep'] != None:
        vector = vector[[kargs['vector_column_to_keep'], 'geometry']]
    else:
        vector = vector[['geometry']]


    if vector.shape[0] > kargs['limit_polygon']:
        groupes = vector.shape[0] / kargs['limit_polygon']

        nb = 0

        for x in range(int(groupes) + 1):
            nb_ = nb + kargs['limit_polygon']

            vector_sample = vector[nb:nb_].copy()
            vector_sample.reset_index(inplace=True)  # sans cela, ne marche pas !

            try:
                geobject = pd.concat([geobject,
                                      calcul_zonal_nc_multiproc(vector_sample, netcdf_folder_path,
                                                                stats)])  # et oui etonnament ca marche...
            except:
                geobject = calcul_zonal_nc_multiproc(vector_sample, netcdf_folder_path, stats)

            nb += kargs['limit_polygon']

    else:
        geobject = calcul_zonal_nc_multiproc(vector, netcdf_folder_path, stats)


    geobject = geobject.sort_index(axis=1)

    if 'tp' in geobject['var'][0]:
        geobject = resample_day_precipitation(geobject)

    return geobject

def save_geobject(vector_path, geobject, stats):

    var = geobject['var'][0] #vient recuperer la première variable car identique aux autres
    name = Path(vector_path).name
    name_compose = f"{var}_{stats}_{name}"
    path = Path(vector_path).parent

    write_geobject(geobject, f"{path}/{name_compose}")

    return print("Product save")


"""

#To execute the process from file ==> desactivate the 'Run with Python consol' or execute in debug mode
if __name__ == '__main__':
    freeze_support()

    vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/CONTOUR-IRIS-MTP_4326.shp"
    # vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/CONTOUR-IRIS-WGS84_74.shp"
    #vector_path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/IRIS/CONTOUR-IRIS-WGS84.shp"
    #vector_path = 'D:\\Mes Donnees\\era5_tasks-main\\shape\\CONTOUR-IRIS-MTP_4326.shp'

    # documentation : https://confluence.ecmwf.int/display/CKB/era5-Land%3A+data+documentation#era5Land:datadocumentation-Dataorganisationandaccess


    path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/era5_netcdf/test"
    #path = "/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/era5_netcdf/era5_tp"
    #path = '/home/bouvier/Documents/GIT_projects/era5_tasks/files_tests/era5_netcdf/era5_t2m'
    #path = "D:\\Mes Donnees\\era5_tasks-main\\netcdf\\era5_t2m"

    stats = "mean"

    ### Processing_elements
    vector_column_to_keep = 'ID'
    
    
    geobject = calculation_process_huge_polygon_multiproc(vector_path, path, stats,
                                                                 limit_polygon=15000, vector_column_to_keep=vector_column_to_keep)
    save_geobject(vector_path, geobject, stats)
    
    
    # Une autre solution serait de combiner tous les netcdf en un seul
    # ensuite calculer les pixels netcdf impactées par les vecteurs
    # calculer la moyenne par pixels directement en array.
    # potentiellement plus rapide
"""
