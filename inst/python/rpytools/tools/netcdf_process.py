from pathlib import Path, PosixPath, WindowsPath
from tools.control_os import whatpathtype

def list_netcdf(path):
    """
    @param path: the directory path of your raster file
    @param raster_files: control the file extension (should be modified by other extent).
    @return: a list of file in your directory
    """
    osdef = whatpathtype()
    files = list()

    if osdef == WindowsPath :
        p = Path(path)
        files = [x for x in p.iterdir() if x.is_file()]

    if osdef == PosixPath :
        p = Path(path).glob('*/')
        files = [x for x in p if x.is_file()]


    raster_files = list(filter(lambda x: ('.nc' in x._str), files))
    return raster_files