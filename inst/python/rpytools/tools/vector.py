import geopandas as gpd
import traceback

def open_vector(pathtovector):

    try:
       vector_in = gpd.read_file(pathtovector)

    except Exception:
       return traceback.print_exc()

    return vector_in

def boundingbox(vectorobject):

    bounds = vectorobject.total_bounds
    bounds_dict = {
        'xmin': bounds[0],
        'ymin': bounds[1],
        'xmax': bounds[2],
        "ymax": bounds[3],
    }
    return bounds_dict

def write_geobject(geobject, path_file):
    """
    @param geobject: geopandas object
    @param path_file: the path to save the geopandas object, e.g. : 'path/name.shp'
    """
    geobject.to_file(path_file)