
# spenvdt

<!-- badges: start -->
<!-- badges: end -->

The goal of spenvdt is to ...

## Installation

You can install the development version of spenvdt like so:

``` r
install.packages(remotes)
library(remotes)
remotes::install_gitlab("umr-astre/spenvdt",   host = "https://forgemia.inra.fr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(spenvdt)
## basic example code

f <- system.file("data/SpatVec.shp", package="spenvdt")

output <- extract_spenvdt(output_path = getwd(),
                          path_input_shp = f,
                          starttime = "2022-03-01",
                          endtime = "2022-03-31")

output               
```

